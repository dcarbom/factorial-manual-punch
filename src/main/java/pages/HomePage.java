package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import utils.SeleniumDriver;

public class HomePage {
	
	String url;

	@FindBy(css="div._32b_p_2-jO.box___nBFPS.flex_direction_column___3_yBK.flexGrow___2-3Hb.gap_s8___2lcN7 > div:nth-child(4) > div > a")
	private WebElement fichaje_button;
	
	public  HomePage()
	{
		PageFactory.initElements(SeleniumDriver.getDriver(), this);
		this.url = SeleniumDriver.getBaseUrlApp() + "dashboard";
	}
	
	public String getUrl()
	{
		return this.url;
		
	}
	
	public void agregar()
	{
		this.fichaje_button.click();
	}
}
