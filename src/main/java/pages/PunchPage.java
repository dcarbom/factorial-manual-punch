package pages;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import utils.SeleniumDriver;

public class PunchPage {

	private String url;
	public static final String hhin=(SeleniumDriver.getConfig().getProperty("hhin") != null)?SeleniumDriver.getConfig().getProperty("hhin"):"0830";
	public static final String hhout=(SeleniumDriver.getConfig().getProperty("hhout") != null)?SeleniumDriver.getConfig().getProperty("hhout"):"1730";
	public static final String hhouteat=(SeleniumDriver.getConfig().getProperty("hhouteat") != null)?SeleniumDriver.getConfig().getProperty("hhouteat"):"1430";
	public static final String hhineat=(SeleniumDriver.getConfig().getProperty("hhineat") != null)?SeleniumDriver.getConfig().getProperty("hhineat"):"1530";
	public static final boolean intensiveDays=(SeleniumDriver.getConfig().getProperty("intensiveDays") != null) && Boolean.parseBoolean(SeleniumDriver.getConfig().getProperty("intensiveDays"));
	public static final String hhinIntensive=(SeleniumDriver.getConfig().getProperty("hhinIntensive") != null)?SeleniumDriver.getConfig().getProperty("hhinIntensive"):"0800";
	public static final String hhoutIntensive=(SeleniumDriver.getConfig().getProperty("hhoutIntensive") != null)?SeleniumDriver.getConfig().getProperty("hhoutIntensive"):"1500";
	public static final String intensiveDaysStart=(SeleniumDriver.getConfig().getProperty("intensiveDaysStart") != null)?SeleniumDriver.getConfig().getProperty("intensiveDaysStart"):"701";
	public static final String intensiveDaysEnd=(SeleniumDriver.getConfig().getProperty("intensiveDaysEnd") != null)?SeleniumDriver.getConfig().getProperty("intensiveDaysEnd"):"831";
	public static final String[] daysToIgnore= SeleniumDriver.getConfig().getProperty("daysToIgnore").split(",");

	public Date today;

	public PunchPage() {
		PageFactory.initElements(SeleniumDriver.getDriver(), this);
		this.today = new Date(System.currentTimeMillis());
		String month = new SimpleDateFormat("M").format(today);
		String year = new SimpleDateFormat("yyyy").format(today);
		this.url = SeleniumDriver.getBaseUrlApp() + "attendance/clock-in/" + year + "/" + month;
	}

	public String getUrl() {
		return this.url;
	}

	public static String getHHin() {
		return hhin;
	}
	public static String getHHout() {
		return hhout;
	}
	public static String getHHoutEat() {
		return hhouteat;
	}
	public static String getHHinEat() {
		return hhineat;
	}
	public static String getHHinIntesive() {
		return hhinIntensive;
	}
	public static String getHHoutIntesive() {
		return hhoutIntensive;
	}

	public boolean insertTime() {
		return fillEmptyPassedDays();
	}

	private boolean fillEmptyPassedDays(){
		boolean daysFilled = false;
		if (daysToIgnore.length > 0){
			System.out.println();
			System.out.println("****************************");
			System.out.println("IGNORED DAYS");
			System.out.println("****************************");
			for (String dayToIgnore : daysToIgnore){
				System.out.println(dayToIgnore);
			}
			System.out.println("****************************");
		}
		System.out.println();
		System.out.println("****************************");
		System.out.println("FILLED DAYS");
		System.out.println("****************************");
		if (this.currentMonthEmpty()){
			this.goToPreviousMonth();
			daysFilled = this.fillEmptyPassedDaysPreviousMonth();
			this.goToNextMonth();
		}
		daysFilled = this.fillEmptyPassedDaysActualMonth();
		System.out.println("****************************");
		return daysFilled;
	}

	private boolean currentMonthEmpty(){
		List<WebElement> days = SeleniumDriver.getDriver().findElements(By.cssSelector("div > table > tbody > tr"));
		WebElement currentFile;
		boolean isEmpty = true;
		int dayToday = Integer.parseInt(new SimpleDateFormat("dd").format(today));
		for (int i = 0; i < dayToday;i++){
			currentFile = days.get(i);
			boolean fileDisabled = currentFile.getAttribute("class").contains("disabled");
			if (!fileDisabled){
				WebElement inputs = currentFile.findElement(By.cssSelector("td:nth-child(2) > div > div > div > div:nth-child(2) > div > div"));
				WebElement enterInput = inputs.findElement(By.cssSelector("div:nth-child(1) > input"));
				String enterInputValue = enterInput.getAttribute("value");
				if (!enterInputValue.equals("")){
					isEmpty = false;
				}
			}
		}
		return isEmpty;
	}

	private boolean fillEmptyPassedDaysActualMonth(){
		boolean dayFilled = false;
		List<WebElement> days = SeleniumDriver.getDriver().findElements(By.cssSelector("div > table > tbody > tr"));
		WebElement currentFile;
		int dayToday = Integer.parseInt(new SimpleDateFormat("dd").format(today));
		for (int i = 0; i < dayToday;i++){
			currentFile = days.get(i);
			boolean fileDisabled = currentFile.getAttribute("class").contains("disabled");
			if (!fileDisabled && !isIgnoredDay(currentFile)){
				WebElement inputs = currentFile.findElement(By.cssSelector("td:nth-child(2) > div > div > div > div:nth-child(2) > div > div"));
				WebElement enterInput = inputs.findElement(By.cssSelector("div:nth-child(1) > input"));
				String enterInputValue = enterInput.getAttribute("value");
				if (enterInputValue.equals("")){
					if (i == dayToday - 1) {
						Date dateNow = new Date(System.currentTimeMillis());
						String hora = new SimpleDateFormat("HH").format(dateNow);
						String min = new SimpleDateFormat("mm").format(dateNow);
						String currentTime = hora + min;
						if (isIntensiveDay(currentFile)){
							if (Integer.parseInt(currentTime) >= Integer.parseInt(hhoutIntensive)) {
								this.fillFile(currentFile, true);
								dayFilled = true;
							}
						}
						else if (Integer.parseInt(currentTime) >= Integer.parseInt(hhout)) {
							this.fillFile(currentFile, false);
							dayFilled = true;
						}
					} else {
						this.fillFile(currentFile, isIntensiveDay(currentFile));
						dayFilled = true;
					}
				}
			}
		}
		return dayFilled;
	}

	private boolean fillEmptyPassedDaysPreviousMonth(){
		boolean dayFilled = false;

		String monthStatusLabel = SeleniumDriver.getDriver().findElement(By.cssSelector("div.label___vXCbg > div")).getText();

		if (!monthStatusLabel.equalsIgnoreCase("aprobado")){
			List<WebElement> days = SeleniumDriver.getDriver().findElements(By.cssSelector("div > table > tbody > tr"));
			WebElement currentFile;
			for (int i = 0; i < days.size();i++){
				currentFile = days.get(i);
				boolean fileDisabled = currentFile.getAttribute("class").contains("disabled");
				if (!fileDisabled && !isIgnoredDay(currentFile)){
					WebElement inputs = currentFile.findElement(By.cssSelector("td:nth-child(2) > div > div:nth-child(1) > div > div:nth-child(2) > div > div:nth-child(1)"));
					WebElement enterInput = inputs.findElement(By.cssSelector("div:nth-child(1) > input"));
					String enterInputValue = enterInput.getAttribute("value");
					if (enterInputValue.equals("")){
						this.fillFile(currentFile, isIntensiveDay(currentFile));
						dayFilled = true;
					}
				}
			}
		}
		return dayFilled;
	}

	private void goToPreviousMonth(){
		String currentUrl = SeleniumDriver.getDriver().getCurrentUrl();
		String[] currentUrlArray = currentUrl.split("/");
		int currentMonth = Integer.parseInt(currentUrlArray[currentUrlArray.length-1]);
		int currentYear = Integer.parseInt(currentUrlArray[currentUrlArray.length-2]);
		String month = currentMonth-1 + "";
		String year = currentYear + "";
		if (currentMonth == 1){
			month = "12";
			year = currentYear-1 + "";
		}
		String urlToGo = SeleniumDriver.getBaseUrlApp() + "attendance/clock-in/" + year + "/" + month;
		SeleniumDriver.openPage(urlToGo);
		SeleniumDriver.waitForPageToLoad(2000);
	}

	private void goToNextMonth(){
		String currentUrl = SeleniumDriver.getDriver().getCurrentUrl();
		String[] currentUrlArray = currentUrl.split("/");
		int currentMonth = Integer.parseInt(currentUrlArray[currentUrlArray.length-1]);
		int currentYear = Integer.parseInt(currentUrlArray[currentUrlArray.length-2]);
		String month = currentMonth+1 + "";
		String year = currentYear + "";
		if (currentMonth == 12){
			month = "1";
			year = currentYear+1 + "";
		}
		String urlToGo = SeleniumDriver.getBaseUrlApp() + "attendance/clock-in/" + year + "/" + month;
		SeleniumDriver.openPage(urlToGo);
		SeleniumDriver.waitForPageToLoad(2000);
	}

	private boolean fillFile(WebElement file, boolean intensiveDay){
		WebElement entrance1 = file.findElement(By.cssSelector("td:nth-child(2) > div > div:nth-child(1)"));
		if (intensiveDay){
			fillEntrance(entrance1, hhinIntensive, hhoutIntensive);
		}
		else{
			fillEntrance(entrance1, hhin, hhouteat);
		}
		SeleniumDriver.waitForPageToLoad(6000);

		if (!intensiveDay){
			WebElement addInputs_button = entrance1.findElement(By.cssSelector("div > div:nth-child(3) > button"));
			addInputs_button.click();

			WebElement entrance2 = file.findElement(By.cssSelector("td:nth-child(2) > div > div:nth-child(2)"));
			fillEntrance(entrance2, hhineat, hhout);
		}
		WebElement dateCurrentFile = file.findElement(By.cssSelector("td:nth-child(1) > div > div:nth-child(1) > span"));
		System.out.println(dateCurrentFile.getText());
		return true;
	}

	private void fillEntrance (WebElement entrance, String hh1, String hh2){
		WebElement inputs = entrance.findElement(By.cssSelector("div > div:nth-child(2) > div"));
		WebElement hh1_input = inputs.findElement(By.cssSelector("div:nth-child(1) > label > div > div > input"));
		WebElement hh2_input = inputs.findElement(By.cssSelector("div:nth-child(3) > label > div > div > input"));
		hh1_input.sendKeys(hh1);
		hh2_input.sendKeys(hh2);
		SeleniumDriver.waitForPageToLoad(1000);
		WebElement guardar_button1 = entrance.findElement(By.cssSelector("div > div:nth-child(2) > div:nth-child(2) > button"));
		guardar_button1.click();
	}

	private boolean isIgnoredDay (WebElement file){
		if (daysToIgnore.length > 0){
			String monthAndDayToFill = getFileMonthAndDay(file);
			for (String dayToIgnore : daysToIgnore){
				if (monthAndDayToFill.equals(dayToIgnore)) return true;
			}
		}
		return false;
	}

	private boolean isIntensiveDay (WebElement file){
		if (intensiveDays){
			Integer monthAndDayToFillInt = Integer.parseInt(getFileMonthAndDay(file));
			if ((monthAndDayToFillInt >= Integer.parseInt(intensiveDaysStart)) && (monthAndDayToFillInt <= Integer.parseInt(intensiveDaysEnd))){
				return true;
			}
		}
		return false;
	}

	private String getFileDate(WebElement file){
		WebElement dateCurrentFile = file.findElement(By.cssSelector("td:nth-child(1) > div > div:nth-child(1) > span"));
		return dateCurrentFile.getText();
	}

	private String monthNameToNumber(String monthName){
		String monthNumber = "";
		switch (monthName){
			case "ene":
				monthNumber = "1";
				break;
			case "feb":
				monthNumber = "2";
				break;
			case "mar":
				monthNumber = "3";
				break;
			case "abr":
				monthNumber = "4";
				break;
			case "may":
				monthNumber = "5";
				break;
			case "jun":
				monthNumber = "6";
				break;
			case "jul":
				monthNumber = "7";
				break;
			case "ago":
				monthNumber = "8";
				break;
			case "sep":
				monthNumber = "9";
				break;
			case "oct":
				monthNumber = "10";
				break;
			case "nov":
				monthNumber = "11";
				break;
			case "dic":
				monthNumber = "12";
				break;
			default:
				monthNumber = "1";
				break;
		}
		return monthNumber;
	}

	public String getFileMonthAndDay(WebElement file){
		String lastDateNonFormatted = this.getFileDate(file);
		String[] dateArray = lastDateNonFormatted.split(" ");
		String day = dateArray[0];
		if (day.length() == 1) day = "0" + day;
		String monthName = dateArray[1].replace(".", "");
		return monthNameToNumber(monthName) + day;
	}

	public boolean isLastEnterIntensiveDay(){
		return intensiveDays && isIntensiveDay(this.getLastEnter());
	}

	public String getLastInTime() {
		WebElement currentFile = this.getLastEnter();
		System.out.println();
		System.out.println("****************************");
		System.out.println("LAST ENTRY");
		System.out.println("****************************");
		System.out.println("DATE: " + getFileDate(currentFile));
		WebElement inputs = currentFile.findElement(By.cssSelector("td:nth-child(2) > div > div:nth-child(1) > div > div:nth-child(2) > div"));
		WebElement enterInput = inputs.findElement(By.cssSelector("div:nth-child(1) > label > div > div > input"));
		String enterInputValue = enterInput.getAttribute("value").replace(":", "");
		System.out.println("In: " + enterInputValue);
		return enterInputValue;
	}

	public String getLastOutEatTime() {
		WebElement currentFile = this.getLastEnter();
		WebElement inputs = currentFile.findElement(By.cssSelector("td:nth-child(2) > div > div:nth-child(1) > div > div:nth-child(2) > div"));
		WebElement enterInput = inputs.findElement(By.cssSelector("div:nth-child(3) > label > div > div > input"));
		String enterInputValue = enterInput.getAttribute("value").replace(":", "");
		System.out.println("OutEat: " + enterInputValue);
		return enterInputValue;
	}

	public String getLastInEatTime() {
		if (this.filesInsertedLastEntry() > 1) {
			WebElement currentFile = this.getLastEnter();
			WebElement inputs = currentFile.findElement(By.cssSelector("td:nth-child(2) > div > div:nth-child(2) > div > div:nth-child(2) > div"));
			WebElement enterInput = inputs.findElement(By.cssSelector("div:nth-child(1) > label > div > div > input"));
			String enterInputValue = enterInput.getAttribute("value").replace(":", "");
			System.out.println("InEat: " + enterInputValue);
			return enterInputValue;
		}
		return null;
	}
	
	public String getLastOutTime() {
		if (this.filesInsertedLastEntry() > 1) {
			WebElement currentFile = this.getLastEnter();
			WebElement inputs = currentFile.findElement(By.cssSelector("td:nth-child(2) > div > div:nth-child(2) > div > div:nth-child(2) > div"));
			WebElement enterInput = inputs.findElement(By.cssSelector("div:nth-child(3) > label > div > div > input"));
			String enterInputValue = enterInput.getAttribute("value").replace(":", "");
			System.out.println("Out: " + enterInputValue);
			System.out.println("****************************");
			return enterInputValue;
		}
		return null;
	}

	public int filesInsertedLastEntry(){
		WebElement currentFile = this.getLastEnter();
		WebElement filesInserted = currentFile.findElement(By.cssSelector("td:nth-child(2) > div"));
		List<WebElement> filesInsertedList = filesInserted.findElements(By.cssSelector("div"));
		if (filesInsertedList.size() > 12) return 2;
		else return 1;
	}

	public WebElement getLastEnter() {
		int dayToday = Integer.parseInt(new SimpleDateFormat("dd").format(today));
		int monthToday = Integer.parseInt(new SimpleDateFormat("M").format(today));
		String currentUrl = SeleniumDriver.getDriver().getCurrentUrl();
		String[] currentUrlArray = currentUrl.split("/");
		int currentMonth = Integer.parseInt(currentUrlArray[currentUrlArray.length-1]);
		final Calendar cal = Calendar.getInstance();
		Date dateNow = new Date(System.currentTimeMillis());
		String hora = new SimpleDateFormat("HH").format(dateNow);
		String min = new SimpleDateFormat("mm").format(dateNow);
		String currentTime = hora + min;
		List<WebElement> days = SeleniumDriver.getDriver().findElements(By.cssSelector("div > table > tbody > tr"));
		Date lastdate = cal.getTime();
		int selectedDay = Integer.parseInt(new SimpleDateFormat("dd").format(lastdate));
		WebElement currentFile = days.get(selectedDay-1);
		if (isIntensiveDay(currentFile) && Integer.parseInt(currentTime) < Integer.parseInt(hhoutIntensive)) {
			if ((dayToday == 1) && (currentMonth == monthToday)){
				goToPreviousMonth();
				days = SeleniumDriver.getDriver().findElements(By.cssSelector("div > table > tbody > tr"));
			}
			cal.add(Calendar.DATE, -1);
		}
		if (!isIntensiveDay(currentFile) && Integer.parseInt(currentTime) < Integer.parseInt(hhout)) {
			if ((dayToday == 1) && (currentMonth == monthToday)){
				goToPreviousMonth();
				days = SeleniumDriver.getDriver().findElements(By.cssSelector("div > table > tbody > tr"));
			}
			cal.add(Calendar.DATE, -1);
		}
		lastdate = cal.getTime();
		selectedDay = Integer.parseInt(new SimpleDateFormat("dd").format(lastdate));
		currentFile = days.get(selectedDay-1);
		boolean fileDisabled = currentFile.getAttribute("class").contains("disabled");
		while (fileDisabled || isIgnoredDay(currentFile)) {
			selectedDay--;
			if (selectedDay <= 0){
				goToPreviousMonth();
				days = SeleniumDriver.getDriver().findElements(By.cssSelector("div > table > tbody > tr"));
				selectedDay = days.size();
			}
			currentFile = days.get(selectedDay-1);
			fileDisabled = currentFile.getAttribute("class").contains("disabled");

		}
		return currentFile;
	}
}
