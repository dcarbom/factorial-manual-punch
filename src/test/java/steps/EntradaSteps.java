package steps;

import org.testng.Assert;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import pages.PunchPage;
import pages.HomePage;
import pages.LoginPage;
import utils.SeleniumDriver;

public class EntradaSteps {

	LoginPage loginPage = new LoginPage();
	HomePage homePage = new HomePage();
	PunchPage punchPage = new PunchPage();
	private static boolean timeInserted = false;
	
	private static final String username = System.getProperty("usr");
	private static final String password = System.getProperty("pwd");
	public static final boolean intensiveDays=(SeleniumDriver.getConfig().getProperty("intensiveDays") != null) && Boolean.parseBoolean(SeleniumDriver.getConfig().getProperty("intensiveDays"));
	
	@Given("^I am on the login page$")
	public void i_am_on_the_login_page() {
	    SeleniumDriver.openPage(this.loginPage.getUrl());
	    SeleniumDriver.waitForPageToLoad(3000);
	    String actualPage = SeleniumDriver.getDriver().getCurrentUrl();
		String expectedPage = this.loginPage.getUrl();
	    Assert.assertEquals(actualPage, expectedPage, "Error al entrar a la pagina de fichaje");
	}
	
	@When("^get into home page$")
	public void get_into_home_page() {
		this.loginPage.usernameInput(EntradaSteps.username);
		this.loginPage.passwordInput(EntradaSteps.password);
		this.loginPage.login();
		SeleniumDriver.waitForPageToLoad(5000);
		String actualPage = SeleniumDriver.getDriver().getCurrentUrl();
		String expectedPage = this.homePage.getUrl();
	    Assert.assertEquals(actualPage, expectedPage, "Error en el login"); 
	}

	@And("^go to fichaje page$")
	public void goToFichajePage() {
		this.homePage.agregar();
		SeleniumDriver.waitForPageToLoad(4000);
		String actualPage = SeleniumDriver.getDriver().getCurrentUrl();
		String expectedPage = this.punchPage.getUrl();
		Assert.assertEquals(actualPage, expectedPage, "No se pudo acceder a la página de fichaje");
	}

	@And("^create a new registry$")
	public void create_a_new_registry() {
		timeInserted = this.punchPage.insertTime();
	}

	@Then("^I have a new registry$")
	public void I_have_new_registry() {
		if (this.punchPage.isLastEnterIntensiveDay()) {
			String lastInTime = this.punchPage.getLastInTime();
			String lastOutEatTime = this.punchPage.getLastOutEatTime();

			if (timeInserted) {
				Assert.assertEquals(lastInTime, PunchPage.getHHinIntesive(), "La hora no coincide o es vacía");
				Assert.assertEquals(lastOutEatTime, PunchPage.getHHoutIntesive(), "La hora no coincide o es vacía");
			}
		} else {
			String lastInTime = this.punchPage.getLastInTime();
			String lastOutEatTime = this.punchPage.getLastOutEatTime();
			String lastInEatTime = this.punchPage.getLastInEatTime();
			String lastOutTime = this.punchPage.getLastOutTime();

			if (timeInserted) {
				Assert.assertEquals(lastInTime, PunchPage.getHHin(), "La hora no coincide o es vacía");
				Assert.assertEquals(lastOutEatTime, PunchPage.getHHoutEat(), "La hora no coincide o es vacía");
				Assert.assertEquals(lastInEatTime, PunchPage.getHHinEat(), "La hora no coincide o es vacía");
				Assert.assertEquals(lastOutTime, PunchPage.getHHout(), "La hora no coincide o es vacía");
			}
		}

	}
}
