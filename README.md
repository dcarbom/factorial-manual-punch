## Run the tests suite
Maven should be installed.

In the root folder of the project, where pom.xml is located, run:

~~~
mvn test -Dusr=<username> -Dpwd=<password>
~~~

You can config some things like browser or in and out time in config.properties file (path: src > test > resources > properties > config.properties)




