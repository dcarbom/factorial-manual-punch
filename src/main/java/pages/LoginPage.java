package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import utils.SeleniumDriver;

public class LoginPage {
	
	String url;

	@FindBy(css="#user_email")
	public WebElement username_input;

	@FindBy(css="#user_password")
	public WebElement password_input;
	
	@FindBy(css="input.js-submit.button.button--brand.button--full-width")
	public WebElement login_button;
	
	
	public  LoginPage()
	{
		PageFactory.initElements(SeleniumDriver.getDriver(), this);
		this.url = SeleniumDriver.getBaseUrl() + "users/sign_in";
	}
	
	public String getUrl()
	{
		return this.url;
		
	}
	
	public void usernameInput(String username)
	{
		this.username_input.sendKeys(username);
		
	}
	
	public void passwordInput(String password)
	{
		this.password_input.sendKeys(password);
		
	}
	
	public void login()
	{
		this.login_button.click();
		
	}
}
